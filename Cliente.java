package chat;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class Cliente extends JFrame {

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private JTextField talk;
	private String ip = "localhost";
	private JTextArea history;
	private JComboBox<String> destination;
	private JComboBox<String> action;
	private NotAdminInterface admin;
	private String nick;
	private Pessoa pessoa;
	private Sala salaAtual;
	private ServerOptionsInterface servidor;
	private int esc;
	private ArrayList<String> options;
	private ArrayList<Pessoa>  pes;
	private ArrayList<String> pessoas;
	private JLabel status;
	private JLabel apelido;
	private String objetoRemoto;
	private boolean mesmoNick;
	private String server;

	public class VerificaExpulsao implements Runnable{
		public void run(){
			try{
				while(true){
					if(salaAtual!=null && pessoa.getIn()){
						int ind = admin.getIndexByRoomName(salaAtual.getNome());
						if(esc!=ind){
							esc = ind;
						}
						if(admin.getSala(esc).getPessoaByNick(pessoa.getNick())==null){
							pessoa.setIn(false);
							salaAtual = null;
							pessoas = null;
							history.setText("");
							status.setText("Status: ocioso");
							apelido.setText("");
							talk.setEnabled(false);
							destination.setEnabled(false);
							action.setEnabled(false);
							JOptionPane.showMessageDialog(history, "Voc� foi expulso(a) da sala.");
						}
					}
					Thread.sleep(2000);
				}
			}catch(Exception e){
				if(salaAtual!=null){
					e.printStackTrace();
					pessoa.setIn(false);
					salaAtual = null;
					pessoas = null;
					history.setText("");
					status.setText("Status: ocioso");
					apelido.setText("");
					talk.setEnabled(false);
					destination.setEnabled(false);
					action.setEnabled(false);
					JOptionPane.showMessageDialog(history, "O servidor caiu. Tente conectar-se a outro.");
				}
			}
		}
	}

	public class AtualizaPessoas implements Runnable{
		public void run(){
			try{
				while(true){
					if(salaAtual!=null && pessoa.getIn()){
						int ind = admin.getIndexByRoomName(salaAtual.getNome());
						if(esc!=ind){
							esc = ind;
						}
						int novoNum = admin.getSala(esc).getNumPessoas();
						//System.out.println(pessoa.getNick()+"   "+(destination.getItemCount())+"   "+novoNum);
						if(destination.getItemCount()<=novoNum){
							ArrayList<Pessoa> ps = admin.getSala(esc).getPessoas();
							//System.out.println(ps);
							for(Pessoa pn : ps){
								if(!pn.getNick().equals(pessoa.getNick()) && !pessoas.contains(pn.getNick())){
									destination.addItem(pn.getNick());
									pessoas.add(pn.getNick());
								}
							}
							salaAtual = admin.getSala(esc);
						}else if(destination.getItemCount()>novoNum){
							ArrayList<String> naSala = new ArrayList<String>();
							for(int i=0;i<destination.getItemCount();i++){
								naSala.add(destination.getItemAt(i));
							}
							ArrayList<String> la = new ArrayList<String>();
							for(Pessoa ao : admin.getSala(esc).getPessoas()){
								la.add(ao.getNick());
							}
							for(String s : naSala){
								if(!s.equals("Todos") && !la.contains(s)){
									destination.removeItem(s);
									pessoas.remove(s);
								}
							}
							salaAtual = admin.getSala(esc);
							//System.out.println(salaAtual.getPessoas());
						}
					}else{
						destination.removeAllItems();
					}
					Thread.sleep(2000);
				}
			}catch(Exception e){
				if(salaAtual!=null){
					e.printStackTrace();
					pessoa.setIn(false);
					salaAtual = null;
					pessoas = null;
					history.setText("");
					status.setText("Status: ocioso");
					apelido.setText("");
					talk.setEnabled(false);
					destination.setEnabled(false);
					action.setEnabled(false);
					JOptionPane.showMessageDialog(history, "O servidor caiu. Tente conectar-se a outro.");
				}
			}
		}
	}

	public class MonitoraMensagens implements Runnable{

		@Override
		public void run() {
			while(true){
				try{
					/*for(Mensagem m : admin.getAllMessages(esc)){
						if(m.getLeitores()!=null){
							///Para provar que todas as mensagens s�o realmente apagadas
							if(!m.getLeitores().isEmpty()){
								System.out.println(m + "  " + m.getLeitores());
							} else{
								System.out.println("ok!");
							}
						}
					}*/
					if(salaAtual!=null){
						admin.isConnected();
						int ind = admin.getIndexByRoomName(salaAtual.getNome());
						if(esc!=ind){
							esc = ind;
						}
						ArrayList<Mensagem> msgs = admin.getMessagesByPessoa(esc, pessoa);
						if(!msgs.isEmpty()){
							for(Mensagem msg : msgs){
								history.append(msg.getTexto());
								history.setCaretPosition(history.getDocument().getLength());
							}
						}
					}
					Thread.sleep(1000);
				}
				catch(Exception e){
					if(salaAtual!=null){
						e.printStackTrace();
						pessoa.setIn(false);
						salaAtual = null;
						pessoas = null;
						history.setText("");
						status.setText("Status: ocioso");
						apelido.setText("");
						talk.setEnabled(false);
						destination.setEnabled(false);
						action.setEnabled(false);
						JOptionPane.showMessageDialog(history, "O servidor caiu. Tente conectar-se a outro.");
					}
				}
			}
		}}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Cliente frame = new Cliente();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Cliente() {
		//RMI
		try{
			ip = InetAddress.getLocalHost().getHostAddress();
			try{
				URL myIP = new URL("http://api.externalip.net/ip/");
				BufferedReader in = new BufferedReader(new InputStreamReader(myIP.openStream()));
				ip = in.readLine() + "/" + ip;
			}catch(Exception e){}
			mesmoNick = false;
			objetoRemoto = "notAdmin";
			server = "localhost";
			this.nick = null;
			pessoa = null;
		} catch(Exception e){
			//e.printStackTrace();
		}

		setResizable(false);
		setTitle("Java RMI Chat Admin Client - by: Jeferson Paiva - All Rights Reserved");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 814, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(12, 13, 772, 44);
		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 3, 0, 0));

		JButton enter = new JButton("Entrar");
		enter.setFont(new Font("Tahoma", Font.BOLD, 12));
		enter.addActionListener(new ActionListener() {
			//Entrando na sala
			public void actionPerformed(ActionEvent arg0) {
				try{
					admin.isConnected();
					if(!pessoa.getIn() && salaAtual==null){
						ArrayList<Sala> salas = admin.getSalas();
						if(salas.isEmpty()){
							JOptionPane.showMessageDialog(history, "N�o existem salas no momento. Crie uma, se for administrador.");
						} else{
							//Criando op��es pra colocar no combo box
							outside:
								while(true){
									options = new ArrayList<String>();
									for(int i=0;i<salas.size();i++){
										options.add((i+1) + " - " + salas.get(i).getNome() + " (" + salas.get(i).getNumPessoas() + ")");
									}
									String escolha = (String) JOptionPane.showInputDialog(history, "Salas dispon�veis", "Escolha uma das salas abaixo",
											JOptionPane.QUESTION_MESSAGE, null, options.toArray(), options.get(0));
									if(escolha!=null){
										while(true){
											boolean t = false;
											esc = Integer.parseInt(escolha.substring(0,1))-1;  //obtendo index da sala escolhida
											if(!mesmoNick){
												pessoa.setNick(JOptionPane.showInputDialog(history, "Digite um nickname:"));
												nick=pessoa.getNick();
											}
											if(pessoa.getNick()!=null){
												for(Pessoa p : admin.getSala(esc).getPessoas()){
													if(pessoa.getNick().equals(p.getNick())){
														t=true;
													}
												}
												if(pessoa.getNick().length()==0){
													continue;
												}
												if(!t){
													pessoa.setIn(true);
													salaAtual = admin.entrarNaSala(esc, pessoa);
													status.setText("Status: logado na sala \"" + salaAtual.getNome() +"\"");
													apelido.setText("Nickname: " + pessoa.getNick());
													servidor.showLogMessage("\"" + pessoa.getNick() + "\" entrou na sala \"" + salaAtual.getNome() + "\"\n", ip);
													admin.enviarMensagem(esc, pessoa, null, "++" + pessoa.getNick() + " acabou de entrar na sala\n");
													history.append(">>" + pessoa.getNick() + " acabou de entrar na sala\n");

													//Atualizando combobox com o nome da galera
													destination.removeAllItems();
													destination.addItem("Todos");
													pes = salaAtual.getPessoas();
													pessoas = new ArrayList<String>();
													for(Pessoa pn : pes){
														if(!pn.getNick().equals(pessoa.getNick())){
															destination.addItem(pn.getNick());
															pessoas.add(pn.getNick());
														}
													}
													new Thread(new MonitoraMensagens()).start();
													new Thread(new AtualizaPessoas()).start();
													new Thread(new VerificaExpulsao()).start();
													talk.setEnabled(true);
													destination.setEnabled(true);
													action.setEnabled(true);
													break outside;
												} else{
													JOptionPane.showMessageDialog(history, "J� existe uma pessoa com este nick na sala. Escolha outro.");
												}
											} else{
												break outside;
											}
										}
									} else{
										break outside;
									}
								}
						}
					} else{
						JOptionPane.showMessageDialog(history, "Voc� deve sair da sala atual antes de entrar em outra.");
					}
				} catch(Exception e){
					JOptionPane.showMessageDialog(history, "Falha na conex�o com o servidor. Verifique as configura��es.");
				}
			}
		});
		panel.add(enter);

		JButton exit = new JButton("Sair da sala");
		exit.addActionListener(new ActionListener() {
			//Saindo da sala
			public void actionPerformed(ActionEvent arg0) {
				try{
					admin.isConnected();
					if(!pessoa.getIn() && salaAtual==null){
						JOptionPane.showMessageDialog(history, "Voc� n�o est� em nenhuma sala. Primeiro entre em uma para depois poder sair");
					} else{
						if(pessoa.getIn()){
							admin.enviarMensagem(esc, pessoa, null, "++" + pessoa.getNick() + " acabou de sair da sala\n");
							admin.sairDaSala(esc, pessoa);
						}
						pessoa.setIn(false);
						history.setText("");
						status.setText("Status: ocioso");
						apelido.setText("");
						talk.setEnabled(false);
						destination.setEnabled(false);
						action.setEnabled(false);
						servidor.showLogMessage("\"" + (pessoa.getNome()!=null ? pessoa.getNome() : pessoa.getNick()) + "\" saiu da sala \"" + salaAtual.getNome() + "\"\n", ip);
						JOptionPane.showMessageDialog(history, "Voc� acabou de sair da sala " + salaAtual.getNome());
						salaAtual = null;
						pessoas = null;
					}
				}catch(Exception e){
					JOptionPane.showMessageDialog(history, "Falha na conex�o com o servidor. Verifique as configura��es.");
				}
			}
		});
		exit.setFont(new Font("Tahoma", Font.BOLD, 12));
		panel.add(exit);

		JButton btnConfiguraes = new JButton("Configurar");
		btnConfiguraes.addActionListener(new ActionListener() {
			//Setup
			public void actionPerformed(ActionEvent arg0) {
				if(pessoa==null || !pessoa.getIn()){
					final JFrame conf = new JFrame();
					final JTextField confSameNickText = new JTextField();

					conf.setTitle("Configura\u00E7\u00F5es");
					conf.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
					conf.setBounds(100, 100, 465, 175);
					JPanel contentPane = new JPanel();
					contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
					conf.setContentPane(contentPane);
					contentPane.setLayout(new GridLayout(4, 4, 10, 2));

					final JLabel confObjeto = new JLabel("Objeto Remoto:");
					confObjeto.setFont(new Font("Tahoma", Font.BOLD, 14));
					confObjeto.setHorizontalAlignment(SwingConstants.CENTER);
					contentPane.add(confObjeto);


					final JTextField confObjText = new JTextField();
					confObjText.setFont(new Font("Tahoma", Font.PLAIN, 14));
					contentPane.add(confObjText);
					confObjText.setText(objetoRemoto);
					confObjText.setColumns(10);

					final JLabel confServidor = new JLabel("Servidor:");
					confServidor.setFont(new Font("Tahoma", Font.BOLD, 14));
					confServidor.setHorizontalAlignment(SwingConstants.CENTER);
					contentPane.add(confServidor);

					final JTextField confServText = new JTextField();
					confServText.setFont(new Font("Tahoma", Font.PLAIN, 14));
					confServText.setText(server);
					contentPane.add(confServText);
					confServText.setColumns(10);

					final JCheckBox confSameNick = new JCheckBox("Usar sempre o mesmo nick");
					confSameNick.setFont(new Font("Tahoma", Font.BOLD, 14));
					confSameNick.setHorizontalAlignment(SwingConstants.CENTER);
					try{
						admin.isConnected();
					} catch(Exception e){
						confSameNick.setEnabled(false);
						confSameNickText.setEnabled(false);
					}
					if(mesmoNick){
						confSameNick.setSelected(true);
					}
					confSameNick.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							if(confSameNick.isSelected()){
								confSameNickText.setEnabled(true);
							} else{
								confSameNickText.setEnabled(false);
							}
						}
					});
					contentPane.add(confSameNick);

					confSameNickText.setFont(new Font("Tahoma", Font.PLAIN, 14));
					if(pessoa!=null && mesmoNick){
						confSameNickText.setText(pessoa.getNick());
					}
					if(!confSameNick.isSelected()){
						confSameNickText.setEnabled(false);
					}
					contentPane.add(confSameNickText);
					confSameNickText.setColumns(10);

					final JButton confSalvar = new JButton("Salvar");
					confSalvar.addActionListener(new ActionListener() {
						//Criando sala
						public void actionPerformed(ActionEvent arg0){
							boolean modi=false;
							if(confSameNick.isSelected()!=mesmoNick || !confSameNickText.getText().equals(nick)){
								if(confSameNick.isSelected()){
									if(!confSameNickText.equals(pessoa.getNick())){
										if(confSameNickText.getText().length()==0){
											JOptionPane.showMessageDialog(conf.getContentPane(), "Digite um nick ou desmarque o check box.");
										} else{
											pessoa.setNick(confSameNickText.getText());
											nick=pessoa.getNick();
											mesmoNick=true;
											modi=true;
										}
									}
								} else{
									if(!confSameNickText.getText().equals("")){
										mesmoNick=false;
										pessoa.setNick(null);
										nick=pessoa.getNick();
										confSameNickText.setText("");
										modi=true;
									}
								}
							}
							if(modi){
								JOptionPane.showMessageDialog(conf.getContentPane(), "Altera��es salvas com sucesso.");
							}
						}
					});
					contentPane.add(confSalvar);

					final JButton confConectar = new JButton("Conectar");
					confConectar.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent arg0) {
							try{
								objetoRemoto=confObjText.getText();
								server=confServText.getText();
								admin = (NotAdminInterface)Naming.lookup("//"+ server + "/" + objetoRemoto);
								servidor = (ServerOptionsInterface)Naming.lookup("//"+server+"/servidor");
								pessoa = new Pessoa(null, nick);
								pessoa.setIn(false);
								confSameNick.setEnabled(true);
								confSameNickText.setEnabled(true);
								JOptionPane.showMessageDialog(conf.getContentPane(), "Conex�o realizada com sucesso.");
							}catch(Exception e){
								//e.printStackTrace();
								JOptionPane.showMessageDialog(conf.getContentPane(), "N�o foi poss�vel realizar a conex�o. Verifique as configura��es.");
								admin=null;
								servidor=null;
								confSameNick.setEnabled(false);
								confSameNickText.setEnabled(false);
							}
						}
					});
					contentPane.add(confConectar);

					conf.setVisible(true);
				}else{
					JOptionPane.showMessageDialog(history, "Saia da sala atual para fazer configura��es.");
				}
			}
		});
		btnConfiguraes.setFont(new Font("Tahoma", Font.BOLD, 12));
		panel.add(btnConfiguraes);

		JPanel panel_1 = new JPanel();
		panel_1.setBounds(12, 83, 772, 264);
		contentPane.add(panel_1);
		panel_1.setLayout(new BorderLayout(0, 0));

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		panel_1.add(scrollPane, BorderLayout.CENTER);

		history = new JTextArea();
		history.setFont(new Font("Tahoma", Font.PLAIN, 16));
		history.setEditable(false);
		history.setLineWrap(true);
		scrollPane.setViewportView(history);

		JPanel panel_2 = new JPanel();
		panel_2.setBounds(12, 356, 772, 35);
		contentPane.add(panel_2);
		panel_2.setLayout(new GridLayout(0, 2, 0, 0));

		action = new JComboBox<String>();
		action.setEnabled(false);
		action.setFont(new Font("Tahoma", Font.PLAIN, 16));
		action.addItem("fala para");
		action.addItem("resmunga para");
		action.addItem("briga com");
		action.addItem("flerta com");
		panel_2.add(action);

		destination = new JComboBox<String>();
		destination.setEnabled(false);
		destination.setFont(new Font("Tahoma", Font.PLAIN, 16));
		destination.addItem("Todos");
		panel_2.add(destination);

		talk = new JTextField();
		talk.setEnabled(false);
		talk.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent k) {
				if(k.getKeyCode()==10 && talk.getText().length()>0){
					String rec = (String)destination.getSelectedItem();
					Pessoa receptor = null;
					String msg;
					if(rec.equals("Todos")){
						receptor = null;
						msg = pessoa.getNick() + " " + action.getSelectedItem() + " todos: " + talk.getText() + "\n";
					}else{
						try {
							receptor = admin.getSala(esc).getPessoaByNick(rec);
						} catch (RemoteException e) {
							//e.printStackTrace();
						}
						msg = pessoa.getNick() + " " + action.getSelectedItem() + " " + receptor.getNick() + ": " + talk.getText() + "\n";
					}
					try {
						admin.enviarMensagem(esc, pessoa, receptor, msg);
						//System.out.println(mmm.getLeitores() + "   " + mmm.isPublica());
					} catch (Exception e) {
						//e.printStackTrace();
					}
					//System.out.println();
					talk.setText("");
					history.append(msg);
				}
			}
		});
		talk.setFont(new Font("Tahoma", Font.PLAIN, 16));
		talk.setBounds(12, 404, 772, 35);
		contentPane.add(talk);
		talk.setColumns(10);

		status = new JLabel("Status: Ocioso");
		status.setFont(new Font("Tahoma", Font.BOLD, 14));
		status.setBounds(12, 60, 375, 20);
		contentPane.add(status);

		apelido = new JLabel("");
		apelido.setFont(new Font("Tahoma", Font.BOLD, 14));
		apelido.setBounds(409, 60, 375, 20);
		contentPane.add(apelido);

		//RMI
		try{
		} catch(Exception e){
			//e.printStackTrace();
		}
	}
}
