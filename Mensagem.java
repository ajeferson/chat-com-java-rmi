package chat;

import java.io.Serializable;
import java.util.ArrayList;

public class Mensagem implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String texto;
	private static int codigo = 0;
	private int id;
	private Pessoa emissor;
	private Pessoa destino;
	private ArrayList<String> leitores;
	private boolean publica;
	
	public Mensagem(String texto, Pessoa emissor, Pessoa destino, ArrayList<String> leitores, boolean publica){
		this.texto = texto;
		this.id = codigo++;
		this.emissor = emissor;
		this.destino = destino;
		this.leitores = leitores;
		this.publica = publica;
	}
	
	public boolean isPublica(){
		return this.publica;
	}
	
	public ArrayList<String> getLeitores(){
		return this.leitores;
	}
	
	public void addLeitor(String nome){
		this.leitores.add(nome);
	}
	
	public String getTexto(){
		return this.texto;
	}
	
	public int getId(){
		return this.id;
	}
	
	public String toString(){
		return this.texto;
	}
	
	public Pessoa getEmissor(){
		return this.emissor;
	}
	
	public Pessoa getReceptor(){
		return this.destino;
	}
	
	public void setTexto(String texto){
		this.texto = texto;
	}
	
	/*public int getIdDestino(){
		return this.idDestino;
	}*/
	
	/*private int getIdEmissor(){
		return this.idEmissor;
	}*/
	
}
