package chat;

import java.rmi.RemoteException;
import java.util.ArrayList;

public interface AdminInterface extends NotAdminInterface{
	
	public Sala addSala(String nomeSala, Pessoa pessoa) throws RemoteException;
	public Sala espionarSala(int indexSala, Pessoa pessoa) throws RemoteException;
	public void cancelSpying(int indexSala, Pessoa pessoa) throws RemoteException;
	public void removeSala(int indexSala) throws RemoteException;
	public void banir(String ip) throws RemoteException;
	public ArrayList<String> getBanidos() throws RemoteException;
	
}
