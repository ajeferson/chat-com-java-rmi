package chat;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface NotAdminInterface extends Remote{
	
	public ArrayList<Sala> getSalas() throws RemoteException;
	public Sala getSala(int index) throws RemoteException;
	public Sala entrarNaSala(int indexSala, Pessoa pessoa) throws RemoteException;
	public Mensagem enviarMensagem(int indexSala, Pessoa emissor, Pessoa receptor, String texto) throws RemoteException;
	public ArrayList<Mensagem> getAllMessages(int indexSala) throws RemoteException;
	public ArrayList<Mensagem> getMessagesByPessoa(int indexSala, Pessoa pessoa) throws RemoteException;
	public void sairDaSala(int indexSala, Pessoa pessoa) throws RemoteException;
	public int getIndexByRoomName(String nome) throws RemoteException;
	public boolean isConnected() throws RemoteException;

}
