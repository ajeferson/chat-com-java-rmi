package chat;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ComboBoxInJOptionPane extends JFrame{
  static final String Select = "Sports";
   String input = "";
  public ComboBoxInJOptionPane() {
    super(Select);
    JButton button=new JButton("Add");
  
  ActionListener lst = new ActionListener() {
    public void actionPerformed(ActionEvent e) {
    String[] sport = new String[] {"Cricket", "FootBall", "Tennis", "Hockey" };
     input = (String) JOptionPane.showInputDialog(ComboBoxInJOptionPane.this,
"Please select your favorite sport",Select, JOptionPane.INFORMATION_MESSAGE, 
null, sport,"Tennis");
     JOptionPane.showMessageDialog(null,"You have selected: "+input);
    }
    };
  System.out.println(input);
  button.addActionListener(lst);
  add(button);
  pack();
    setVisible(true);

  }
  public static void main(String argv[]) {
    new ComboBoxInJOptionPane();
  }
}
