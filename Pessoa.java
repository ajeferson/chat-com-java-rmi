package chat;

import java.io.Serializable;

public class Pessoa implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String nick;
	private String nome;
	private boolean in;
	
	public Pessoa(String nome, String nick){
		this.nick = nick;
		this.nome = nome;
		this.in = false;
	}
	
	public String getNick(){
		return this.nick;
	}
	
	public void setNick(String nick){
		this.nick = nick;
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String toString(){
		return this.getNome();
	}
	
	public boolean getIn(){
		return this.in;
	}
	
	public void setIn(boolean in){
		this.in = in;
	}
	
}
