package chat;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Sala implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String nome;
	private ArrayList<Mensagem> mensagens;
	private ArrayList<Pessoa> pessoas;
	private Pessoa criador;
	private final int id;
	private Pessoa admin;
	
	public Sala(String nome, Pessoa criador, int id){
		this.nome = nome;
		this.criador = criador;
		this.mensagens = new ArrayList<Mensagem>();
		this.pessoas = new ArrayList<Pessoa>();
		this.id = id;
		this.admin = null;
	}
	
	public Pessoa getCriador(){
		return this.criador;
	}
	
	public Pessoa getAdmin(){
		return this.admin;
	}
	
	public void addAdmin(Pessoa admin){
		this.admin = admin;
	}
	
	public void removeAdmin(Pessoa admin){
		this.admin = null;
	}
	
	public ArrayList<Mensagem> getMensagens(){
		return this.mensagens;
	}
	
	public String toString(){
		return this.nome;
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public void addPessoa(Pessoa pessoa){
		this.pessoas.add(pessoa);
	}
	
	public ArrayList<Pessoa> getPessoas(){
		return this.pessoas;
	}
	
	public int getId(){
		return this.id;
	}
	
	public int getNumPessoas(){
		return this.pessoas.size();
	}
	
	public void entrarNaSala(Pessoa pessoa){
		int index=this.pessoas.size();
		for(int i=0;i<this.pessoas.size();i++){
			if(this.pessoas.get(i)==null){
				index = i;
			}
		}
		this.pessoas.add(index, pessoa);
	}
	
	public Mensagem sendMensage(int indexSala, Pessoa emissor, Pessoa receptor, String texto){
		ArrayList<String> p = new ArrayList<String>();
		boolean publica;
		if(receptor==null){
			publica = true;
			for(Pessoa pe : this.pessoas){
				if(!pe.getNick().equals(emissor.getNick())){
					p.add(pe.getNick());
				}
			}
		}else{
			//p = null;
			publica = false;
			p.add(receptor.getNick());
		}
		if(this.admin!=null){
			p.add(this.admin.getNome());
		}
		Mensagem m = new Mensagem(texto, emissor, receptor, p, publica);
		this.mensagens.add(m);
		return m;
	}
	
	public ArrayList<Mensagem> getMessagesByPessoa(Pessoa pessoa){
			ArrayList<Mensagem> msgs = new ArrayList<Mensagem>();
		synchronized (this.mensagens) {
			for(Iterator<Mensagem> it = this.mensagens.iterator(); it.hasNext();){
				Mensagem msg = it.next();
				if(!pessoa.getIn() && msg.getLeitores().contains(pessoa.getNome())){
					msgs.add(msg);
					msg.getLeitores().remove(pessoa.getNome());
					if(msg.getLeitores().isEmpty()){
						it.remove();
					}
				}
				else if(pessoa.getIn() && msg.getLeitores().contains(pessoa.getNick())){
					String previous = msg.getTexto();
					if(!msg.isPublica()){
						msg.setTexto("**" + msg.getTexto());
					}
					msgs.add(new Mensagem(msg.getTexto(), msg.getEmissor(), pessoa, null, false));
					msg.setTexto(previous);
					msg.getLeitores().remove(pessoa.getNick());
					if(msg.getLeitores().isEmpty()){
						it.remove();
					}
				}

			}
			return msgs;
		}
	}
	
	public Pessoa getPessoaByNick(String nome){
		for(Pessoa p : this.pessoas){
			if(p.getNick().equals(nome)){
				return p;
			}
		}
		return null;
	}
	
	public void sairDaSala(Pessoa pessoa){
		synchronized (this.pessoas) {
			if(pessoa==null){
				this.pessoas.clear();
			} else{
				String nick = pessoa.getNick();
				for(Iterator<Pessoa> it = this.pessoas.iterator();it.hasNext();){
					Pessoa p = it.next();
					if(p.getNick().equals(nick)){
						it.remove();
					}
				}
			}
		}
	}
	
	public void clear(){
		this.pessoas.clear();
		this.admin=null;
	}
}
