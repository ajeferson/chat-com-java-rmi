package chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class NotAdmin extends UnicastRemoteObject implements NotAdminInterface{
	
	private static final long serialVersionUID = 1L;
	
	static ArrayList<Sala> salas;
	
	public NotAdmin() throws RemoteException{
		salas = new ArrayList<Sala>();
	}
	
	public ArrayList<Sala> getSalas(){
		return salas;
	}
	
	public Sala getSala(int index){
		return salas.get(index);
	}
	
	public Sala entrarNaSala(int indexSala, Pessoa pessoa){
		salas.get(indexSala).entrarNaSala(pessoa);
		return this.getSala(indexSala);
	}
	
	public Mensagem enviarMensagem(int indexSala, Pessoa emissor, Pessoa receptor, String texto){
		return salas.get(indexSala).sendMensage(indexSala, emissor, receptor, texto);
	}
	
	public ArrayList<Mensagem> getAllMessages(int indexSala){
		return salas.get(indexSala).getMensagens();
	}
	
	public ArrayList<Mensagem> getMessagesByPessoa(int indexSala, Pessoa pessoa){
		return salas.get(indexSala).getMessagesByPessoa(pessoa);
	}
	
	public void sairDaSala(int indexSala, Pessoa pessoa){
		salas.get(indexSala).sairDaSala(pessoa);
	}
	
	public int getIndexByRoomName(String nome){
		for(int i=0;i<salas.size();i++){
			if(salas.get(i).getNome().equals(nome)){
				return i;
			}
		}
		return -1;
	}
	
	public boolean isConnected(){
		return true;
	}
}
