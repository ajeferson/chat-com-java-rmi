package chat;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URL;
import java.rmi.Naming;
import java.rmi.server.UnicastRemoteObject;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class Servidor extends JFrame{

	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	Admin admin;
	NotAdmin notAdmin;
	ServerOptionsInterface servidor;
	JTextArea logs;
	private String ip;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Servidor frame = new Servidor();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Servidor(String nada){}
	
	/**
	 * Create the frame.
	 */
	
	public Servidor() {
		try{
			ip = InetAddress.getLocalHost().getHostAddress();
			URL myIP = new URL("http://api.externalip.net/ip/");
			BufferedReader in = new BufferedReader(new InputStreamReader(myIP.openStream()));
		    ip = in.readLine() + "/" + ip;
		}catch(Exception e){}
		admin = null;
		notAdmin = null;
		servidor = null;
		setTitle("Java RMI Chat Server - by: Jeferson Paiva - All Rights Reserved");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 758, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(12, 13, 716, 37);
		contentPane.add(panel_1);
		panel_1.setLayout(new GridLayout(0, 2, 0, 0));
		
		JButton startButton = new JButton("Start Server");
		startButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					if(admin==null){
						admin = new Admin();
						notAdmin = new NotAdmin();
						servidor = new ServerOptions(logs);
					} else{
						UnicastRemoteObject.exportObject(admin);
						UnicastRemoteObject.exportObject(notAdmin);
						UnicastRemoteObject.exportObject(servidor);
					}
					Naming.rebind("//localhost/admin", admin);
					Naming.rebind("//localhost/notAdmin", notAdmin);
					Naming.rebind("//localhost/servidor", servidor);
					servidor.showLogMessage("Servidor Iniciado!\n", ip);
				}catch(Exception e){
					try{
						servidor.showLogMessage("Problemas na inicializa��o do servidor. Verifique a conex�o.\n", ip);
					}catch(Exception ej){}
				}
			}
		});
		startButton.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel_1.add(startButton);
		
		JButton stopButton = new JButton("Stop Server");
		stopButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try{
					Naming.unbind("//localhost/admin");
					Naming.unbind("//localhost/notAdmin");
					Naming.unbind("//localhost/servidor");
					UnicastRemoteObject.unexportObject(admin, true);
					UnicastRemoteObject.unexportObject(notAdmin, true);
					UnicastRemoteObject.unexportObject(servidor, true);
					for(Sala sala : notAdmin.getSalas()){
						sala.clear();
					}
					servidor.showLogMessage("Servidor Parado!\n", ip);
				}catch(Exception e){
					try{
						servidor.showLogMessage("Problema na paraliza��o do servidor. Verifique a" +
							" conex�o ou se o servidor j� encontra-se paralizado.\n", ip);
					} catch(Exception y){}
				}
			}
		});
		stopButton.setFont(new Font("Tahoma", Font.BOLD, 18));
		panel_1.add(stopButton);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(12, 63, 716, 357);
		contentPane.add(panel_2);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JLabel lblLogs = new JLabel("Logs:");
		lblLogs.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogs.setFont(new Font("Tahoma", Font.BOLD, 16));
		panel_2.add(lblLogs, BorderLayout.NORTH);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		panel_2.add(scrollPane_1, BorderLayout.CENTER);
		
		logs = new JTextArea();
		logs.setFont(new Font("Tahoma", Font.PLAIN, 16));
		logs.setEditable(false);
		scrollPane_1.setViewportView(logs);
	}
}
