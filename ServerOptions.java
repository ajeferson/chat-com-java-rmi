package chat;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JTextArea;

public class ServerOptions extends UnicastRemoteObject implements ServerOptionsInterface{
	
	private static final long serialVersionUID = 1L;
	
	JTextArea logs;
	
	public ServerOptions(JTextArea logs) throws RemoteException{
		this.logs = logs;
	}
	
	public void showLogMessage(String message, String ip){
		DateFormat dia = new SimpleDateFormat("dd/M/yyyy");
		DateFormat hora = new SimpleDateFormat("HH:mm:s");
		logs.append("["+dia.format(new Date())+" - " + hora.format(new Date()) + "] {"+ip+"} " + message);
		logs.setCaretPosition(logs.getDocument().getLength());
	}

}
