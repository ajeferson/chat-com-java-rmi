package chat;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerOptionsInterface extends Remote{

	public void showLogMessage(String message, String ip) throws RemoteException;
	
}
