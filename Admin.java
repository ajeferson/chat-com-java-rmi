package chat;

import java.rmi.RemoteException;
import java.util.ArrayList;

public class Admin extends NotAdmin implements AdminInterface{
	
	private static final long serialVersionUID = 1L;
	private ArrayList<String> banidos;

	public Admin() throws RemoteException{
		this.banidos = new ArrayList<String>();
	}
	
	public Sala addSala(String nomeSala, Pessoa pessoa){
		if(salas.isEmpty()){
			salas.add(new Sala(nomeSala, pessoa, 0));
		} else{
			salas.add(new Sala(nomeSala, pessoa, salas.get(salas.size()-1).getId()+1));
		}
		return salas.get(salas.size()-1);
	}

	public Sala espionarSala(int indexSala, Pessoa pessoa){
		salas.get(indexSala).addAdmin(pessoa);
		return salas.get(indexSala);
	}
	
	public void cancelSpying(int indexSala, Pessoa pessoa){
		salas.get(indexSala).removeAdmin(pessoa);
	}

	public void removeSala(int indexSala) throws RemoteException {
		salas.remove(indexSala);		
	}

	@Override
	public void banir(String ip){
		this.banidos.add(ip);
	}

	@Override
	public ArrayList<String> getBanidos(){
		return this.banidos;
	}
	
}
